# Vircadia (Codename Athena)

### What is this?

Vircadia™ is a 3D social software project seeking to incrementally bring about a truly free and open metaverse, in desktop and XR.

### [Website](https://vircadia.com/) | [Discord](https://discordapp.com/invite/Pvx2vke) | [Download](https://vircadia.com/download-vircadia/)

### Releases

[View Releases here](https://github.com/vircadia/vircadia/releases/)

### How to build the Interface

- [For Windows](https://github.com/vircadia/vircadia/blob/master/BUILD_WIN.md)
- [For Mac](https://github.com/vircadia/vircadia/blob/master/BUILD_OSX.md)
- [For Linux](https://github.com/vircadia/vircadia/blob/master/BUILD_LINUX.md)
- [For Linux - Vircadia Builder](https://github.com/vircadia/vircadia-builder)

### How to deploy a Server

- [For Windows and Linux](https://vircadia.com/deploy-a-server/)

### How to build a Server

- [For Windows](https://github.com/vircadia/vircadia/blob/master/BUILD_WIN.md)
- [For Linux](https://github.com/vircadia/vircadia/blob/master/BUILD_LINUX.md)
- [For Linux - Vircadia Builder](https://github.com/vircadia/vircadia-builder)

### How to generate an Installer

- [For Windows - Interface & Server](https://github.com/vircadia/vircadia/blob/master/INSTALLER.md)
- [For Mac - Interface](https://github.com/vircadia/vircadia/blob/master/INSTALLER.md#os-x)
- [For Linux - Server .deb - Vircadia Builder](INSTALLER.md#ubuntu-1804--deb)
- [For Linux - Server .rpm - Vircadia Builder](INSTALLER.md#amazon-linux-2--rpm)
- [For Linux - Interface AppImage - Vircadia Builder](https://github.com/vircadia/vircadia-builder/blob/master/README.md#building-appimages)

### Boot to Metaverse: [The Goal](https://vircadia.com/vision/)

Having a place to experience adventure, a place to relax with calm breath, that's a world to live in. An engine to support infinite combinations and possibilities of worlds without censorship and interruption, that's a metaverse. Finding a way to make infinite realities our reality is the dream.

### Boot to Metaverse: The Technicals

Vircadia consists of many projects and codebases with its unifying structure's goal being a decentralized metaverse.

- The Interface (Codename Athena) - You are here!
- The Server (Codename Athena) - You are also here!
- The UI Framework (Codename Nyx) - Codebase coming soon.
- [The Metaverse (Codename Iamus)](https://github.com/vircadia/Iamus/)
- [The Metaverse Dashboard (Codename Iamus)](https://github.com/vircadia/project-iamus-dashboard/)
- [The Launcher (Codename Pantheon)](https://github.com/vircadia/pantheon-launcher/)

#### Child Projects
- [Vircadia Builder for Linux](https://github.com/vircadia/vircadia-builder/)
- [General Documentation](https://github.com/vircadia/vircadia-docs-sphinx/)

### Contribution

There are many contributors to Vircadia. Code writers, reviewers, testers, documentation writers, modelers, and general supporters of the project are all integral to its development and success towards its goals. Find out how you can [contribute](CONTRIBUTING.md)!
